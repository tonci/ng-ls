﻿/// <reference path="/ng-app/lib/angular/angular.js" />
/// <reference path="/ng-app/lib/breezejs/breeze.debug.js" />

(function () {
    'use strict';


    var injectParams = ['lightSwitchEntityManager','logger'];
    var customerServiceFactory = function (lightSwitchEntityManager,logger) {
        //private

        var em = lightSwitchEntityManager.Manager();
        var entitySetName = "Customers";
        var entityTypeName = "Customer";

        var getCustomers = function () {
            var query = breeze.EntityQuery.from(entitySetName).orderBy("lastName");//.where("lastName", "contains", "bladel");
            return em.executeQuery(query);

        };
        var hasChanges = function(){
            return em.hasChanges();
        };

        var createCustomer = function () {
            return em.createEntity(entityTypeName);
        };


        var saveChanges = function () {
            em.saveChanges().then(function () {
                logger.info("succesfully saved...")
            }, function (error) {
                //TODO this is just a quick and dirty version for gettingn server side errors in the angular client
                //Is lightSwitch specific formatting, needs to be moved to provider !

                var errorMsgXml = error.body['odata.error'].message.value;
                var messageXML = $.parseXML(errorMsgXml);
                var errorMessage = "Unexpected error";
                if (messageXML) {
                    jMessageXML = $(messageXML);
                    var validationResults = jMessageXML.find("ValidationResult");

                    if (validationResults.length > 0) {
                        errorMessage = "";
                        $.each(validationResults, function (i, validationResult) {
                            var jValidationResult = $(validationResult);
                            var validationResultMessage = jValidationResult.find("Message").text();
                            errorMessage += validationResultMessage;
                        });
                    };

                };


                logger.error("Error during save " + errorMessage);
            });
        };


        //function getMetadata() {
        //    var store = em.metadataStore;
        //    if (store.hasMetadataFor(lightSwitchEntityManager.serviceName)) {
        //        return $q.when(true);
                
        //    }
        //    else { //Get metadata
        //        return store.fetchMetadata(lightSwitchEntityManager.serviceName);
        //    }
        //}


        //public 
        var factory = {
            getCustomers: getCustomers,
            hasChanges: hasChanges,
            createCustomer: createCustomer,
            saveChanges: saveChanges
        };

        return factory;
    };

    customerServiceFactory.$inject = injectParams;

    angular.module('app').factory('customerService', customerServiceFactory)

}());