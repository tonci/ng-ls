﻿(function () {
    'use strict';

    var injectParams = ['$scope', '$modalInstance', 'item'];

    var customerEditController = function ($scope, $modalInstance, item) {


        $scope.item = item;

        $scope.ok = function () {
            $modalInstance.close(item);
        };

        $scope.cancel = function () {
            $scope.item.entityAspect.rejectChanges();
            var dismissId = null;
            if (item.isVirgin) {
                dismissId = item.id;
            }

            $modalInstance.dismiss(dismissId);
        };

        $scope.isUnchanged = function (item) {
            return false //tODO not used currently
        };
    };

    customerEditController.$inject = injectParams;

    angular.module("app").controller("customerEditController", customerEditController);

}());