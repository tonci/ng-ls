﻿/// <reference path="/ng-app/lib/angular/angular.js" />
/// <reference path="/ng-app/lib/breezejs/breeze.debug.js" />

(function () {
    'use strict';

    var injectParams = ['$scope', '$window', '$modal', 'appService', 'logger', 'customerService'];
    var CustomerController = function ($scope, $window, $modal, appService, logger, customerService) {

        var entityTypeName = "Customer";
        var entitySetName = "Customers";
        var summaryProperty = "lastName";
        $scope.items = [];


        //var query = breeze.EntityQuery.from(entitySetName).orderBy("lastName");//.where("lastName", "contains", "bladel");
        customerService.getCustomers().then(function (data) {
            $scope.items = data.results;
        });


        $scope.hasChanges = function () {
            return customerService.hasChanges();
        };

        //TODO using here entityAspect which is basically breezeJS functionality (DI ok?)
        $scope.isUndoable = function (entity) {
            var entityState = entity.entityAspect.entityState;

            if (entityState.isAdded()) {
                return false;
            }

            return (!entityState.isUnchanged());
        };

        $scope.delete = function (entity) {
            entity.entityAspect.setDeleted();
            var index = $scope.items.indexOf(entity)
            $scope.items.splice(index, 1);
        };

        $scope.openHtmlClient = function () {
            $window.location.href = appService.getDeploymentSafeBasePath() + "HtmlClient";
        };
        $scope.addOrEdit = function (entity) {

            if (!entity) {
                entity = customerService.createCustomer();
                entity.isVirgin = true;
                $scope.items.unshift(entity); // newly added records are best on top

            }
            else {
                entity.isVirgin = false;
            };

            var modalInstance = $modal.open({
                templateUrl: appService.getDeploymentSafeBasePath() + 'ng-app/customer/customerEdit.html',
                controller: 'customerEditController',
                size: 'lg',
                resolve: {
                    item: function () {
                        return entity;
                    }
                }
            });

            modalInstance.result.then(function (entity) {
                if (entity.entityAspect.entityState.isAdded()) {
                    logger.info("New " + entityTypeName + " added : " + entity[summaryProperty]);
                }
                else {
                    logger.info(entityTypeName + " updated : " + entity[summaryProperty]);

                }
            }, function (dismissedId) {
                //cancelled, so 
                //TODO : we are removing here the entity, that's NOK, we only want this when an INITIAL add is canceled.
                // quite dificult to identity an initial create :)
                if (dismissedId) {
                    $scope.items.splice(_.indexOf($scope.items, _.findWhere($scope.items, { id: dismissedId })), 1);
                }

            });

        };

        $scope.saveChanges = function () {
            customerService.saveChanges();
        };

        $scope.undoChanges = function (entity) {
            entity.entityAspect.rejectChanges();
        };

    };

    CustomerController.$inject = injectParams;
    angular.module("app").controller("customerController", CustomerController);






}());

//lightSwitchEntityManagerFactory.Manager().fetchMetadata();
//could retrieve meta data earlier...
// use promise