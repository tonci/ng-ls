﻿/// <reference path="/ng-app/lib/angular/angular.js" />
(function () {
    'use strict';
    var appServiceFactory = function () {
        //private
        var appName = "CrudGrid";

        //Constructors for breezeJS computed props
        var Customer = function () {

        };

        Object.defineProperty(Customer.prototype, 'fullName', {
            enumerable: true,
            configurable: true,
            get: function () { return this.firstName + ' ' + this.lastName; }
            // no setter
        });
        function getDeploymentSafeBasePath() {
            //solves the problem that when deployed on IIS relative paths needs also the appName
            //we check here if the url contains 'localhost' to check if we are in debug mode (in visual studio)
            //this only gives trouble when you deploy to localhost, because the app will think it's running inside visual studio
            //so, if you deploy to localhost, use 127.0.0.1 as address
            var origin = location.protocol + '//' + location.host + '/';
            var projectName = location.hostname === 'localhost' ? '' : appName + '/';
            return origin + projectName;
        };

        //public
        var factory = {
            getDeploymentSafeBasePath: getDeploymentSafeBasePath,
            Customer:Customer
        };

        return factory;
    };

    angular.module("app").factory('appService', appServiceFactory);

}());