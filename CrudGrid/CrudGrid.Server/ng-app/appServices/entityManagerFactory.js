﻿(function () {
    'use strict';


    var injectParams = ['breeze','appService'];
    var lightSwitchEntityManagerFactory = function (breeze, appService) {
        breeze.NamingConvention.camelCase.setAsDefault();
        breeze.config.initializeAdapterInstance('dataService', 'OData', true);

        var oldClient = OData.defaultHttpClient;

        var myClient = {
            request: function (request, success, error) {
                if (request.requestUri.indexOf("$metadata", request.requestUri.length - "$metadata".length) !== -1) {
                    request.headers.Accept = "application/xml";
                }
                else {
                    if (request.method === "POST") {
                        var body = request.body;
                        request.body = replaceAll(body, "\r\nDataServiceVersion: 2.0\r\n", "\r\nDataServiceVersion: 3.0\r\nPrefer: return-content\r\n");
                    }
                }
                return oldClient.request(request, success, error);
            }
        };

        OData.defaultHttpClient = myClient;

        var serviceName = appService.getDeploymentSafeBasePath() + "ApplicationData.svc";

        var factory = {
            Manager: function () {
                //return new breeze.EntityManager(serviceName);
                var em = new breeze.EntityManager(serviceName);
                var store = em.metadataStore;

                // register here constructors for types which are enriched with computed props
                store.registerEntityTypeCtor("Customer", appService.Customer);



                return em;
            },
            serviceName: serviceName
        };

        return factory;
    };


    lightSwitchEntityManagerFactory.$inject = injectParams;

    angular.module('app').factory('lightSwitchEntityManager', lightSwitchEntityManagerFactory);

    function escapeRegExp(string) {
        return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    };

    function replaceAll(string, find, replace) {
        return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
    }

}());


