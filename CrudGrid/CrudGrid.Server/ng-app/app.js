﻿(function () {
    'use strict';

    var app = angular.module("app", ['breeze.angular', 'ui.bootstrap']);

    //TODO add ngRoute or ui-route
    //app.config(['$routeProvider', function ($routeProvider) {
    //    var viewBase = '/app/customersApp/views/';

    //    $routeProvider
    //        .when('/customers', {
    //            controller: 'CustomersController',
    //            templateUrl: viewBase + 'customers/customers.html'
    //        })
    //        .when('/login/:redirect*?', {
    //            controller: 'LoginController',
    //            templateUrl: viewBase + 'login.html'
    //        })
    //        .otherwise({ redirectTo: '/customers' });

    //}]);

    //centralised exception handling
    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        exception.message = "An error occured: " +
                                                                exception.message;
                        $delegate(exception, cause);
                        alert(exception.message); //TODO using toastr here (as a service) seems quite strange
                    };
                }]);
    });

    app.run(['breeze', 'lightSwitchEntityManager', function (breeze, lightSwitchEntityManager) {
        //currently nothing to do

        //var em = lightSwitchEntityManagerFactory.Manager();
       
    }]);
}());
