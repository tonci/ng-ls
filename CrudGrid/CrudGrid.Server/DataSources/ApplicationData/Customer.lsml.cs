﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.LightSwitch;
namespace LightSwitchApplication
{
    public partial class Customer
    {
        partial void RowVersionString_Compute(ref string result)
        {
            // Set result to the desired field value

            for (int i = 0; i < this.RowVersion.Length; i++)
            {
                result += this.RowVersion[i];
            }


        }

        partial void LastName_Validate(EntityValidationResultsBuilder results)
        {
            // results.AddPropertyError("<Error-Message>");
            if (this.LastName!=null && this.LastName.StartsWith("x"))
            {
                results.AddEntityError("no x");
            }
        }
    }
}
