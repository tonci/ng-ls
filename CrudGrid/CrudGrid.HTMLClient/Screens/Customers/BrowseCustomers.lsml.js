﻿/// <reference path="~/GeneratedArtifacts/viewModel.js" />

myapp.BrowseCustomers.created = function (screen) {
    // Write code here.

};
myapp.BrowseCustomers.ReturnToNgApp_execute = function (screen) {
    // Write code here.

    var appName = "CrudGrid";

    window.location.href = getDeploymentSafeBasePath() + "ng-app/index.html";


    function getDeploymentSafeBasePath() {
        //solves the problem that when deployed on IIS relative paths needs also the appName
        //we check here if the url contains 'localhost' to check if we are in debug mode (in visual studio)
        //this only gives trouble when you deploy to localhost, because the app will think it's running inside visual studio
        //so, if you deploy to localhost, use 127.0.0.1 as address
        var origin = location.protocol + '//' + location.host + '/';
        var projectName = location.hostname === 'localhost' ? '' : appName + '/';
        return origin + projectName;
    };

};

