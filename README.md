Fork from https://code.msdn.microsoft.com/ENRICHING-LIGHTSWITCH-WITH-1ec9a2d2 by Paul Van Bladel

- [ENRICHING LIGHTSWITCH WITH AN ANGULARJS CLIENT (PART 1 OF 2)](http://blog.pragmaswitch.com/?p=1797)
- [ENRICHING LIGHTSWITCH WITH AN ANGULARJS CLIENT: A SIMPLE CRUD GRID (PART 2 OF 2)](http://blog.pragmaswitch.com/?p=1820)
- upgraded with VS2015

# ENRICHING LIGHTSWITCH WITH AN ANGULARJS CLIENT: A SIMPLE CRUD GRID

- Technologies: Visual Studio LightSwitch, AngularJS
- Topics: CRUD
- Platforms: Web
- Updated: 12/14/2014
- License: [MIT](license.rtf) 
- Sample: [View this sample online](https://code.msdn.microsoft.com/ENRICHING-LIGHTSWITCH-WITH-1ec9a2d2)

## Introduction

I succeeded in adding a page to an existing LightSwitch app containing the very beginning of a Angular based app.

I’ll elaborate first on the mindset and provide the link where you can test this first version.

## This is not an angularJS course

There are plenty of excellent resources on the web where you can learn AngularJS. I’ll focus in this series only topics where I propose a different practice and on topics which are really LightSwitch specific.

In this post, I will simply present the app, which currently contains a simple CRUD grid. In this version, we have only one page with one view, so no menu structure nor client side navigation. Also the grid is just a basic grid without paging. Nonetheless, I tried to focus on the integration of BreezeJS because having a decent service proxy with client side unit of work functionality and state tracking is really a core aspect of a rich client line of business application.

Obviously in later posts, more features will be added

## Show me the link to the example app.

[http://app.pragmaswitch.com/CrudGrid/ng-app/index.html](http://app.pragmaswitch.com/CrudGrid/ng-app/index.html "http://app.pragmaswitch.com/CrudGrid/ng-app/index.html")

## Safeguarding a pristine LightSwitch application

As explained in the previous post, the LightSwitch application keeps as clean as possible. So, I’m not adding web api’s or whatsoever to the LightSwitch server project. The new Angular client simply consumes directly the LightSwitchc OData Service.

## Circular navigation

Currently the sample allows to jump from the LightSwitch app to the Angular page and visa versa.

From the Angular page to lightSwitch:

[![image](description/image_thumb6.png "image")](http://blog.pragmaswitch.com/wp-content/uploads/2014/09/image6.png)

From the LightSwitch app to the Angular page:

[![image](description/image_thumb7.png "image")](http://blog.pragmaswitch.com/wp-content/uploads/2014/09/image7.png)

## CRUD aspects

The Customer list is presented in a simple table and creating and editing new records is done via a modal window:

[![image](description/image_thumb8.png "image")](http://blog.pragmaswitch.com/wp-content/uploads/2014/09/image8.png)

The data binding and model validation is triggered “as you type”. In LightSwitch you need a lost focus on a textbox in order to trigger the validation of the content.

For example, the next screens shows that as long as there is no text in the last name textbox, the OK button will stay disabled:

[![image](description/image_thumb9.png "image")](http://blog.pragmaswitch.com/wp-content/uploads/2014/09/image9.png)

From the moment you type the first character in the last name textbox, the ok button will become enabled (under the assumption there are no other validation errors).

## State tracking with simple undo functionality when editing a record

In following example the third record has been edited.

As a result, the SAVE button colored red indicating that the user should press save changes.

There is also an undo button, in such a way the user can go back to the previous saved state for a particular record.

[![image](description/image_thumb10.png "image")](http://blog.pragmaswitch.com/wp-content/uploads/2014/09/image10.png)

In the above example, when the user presses the undo button for the third record, the SAVE button will color again Blue.

## True computed properties

The Full name property is a genuine computed property meaning it’s defined only ONCE on the entity level. As a result, the property is available on all screens where customer data are relevant and the computation code is in one place.

## Server side calculated and concurrency management fields

Server side calculated fields and concurrency management fields are correctly digested by the client. In the sample, simply note the behavior of the created and modified field. See my previous  posts on BreezeJS for more details.

## Efficient client side unit of work management

BreezeJS makes sure that inserts, updates and deletes are nicely bundled in an Odata batch in such a way strictly the necessary  data and nothing more goes over the wire.

